import java.util.Stack;

class Solution {
    public boolean isValid(String s) {
        Stack<Character> stack = new Stack<Character>();
        //s = "(){}"

        for (char c : s.toCharArray()) {
            if (c == '(' || c == '[' || c == '{') {
                stack.push(c);
            } else {
                if ( stack.isEmpty()) {
                    return false;
                }
                char inStack = stack.peek();
                if ((c == ')' && inStack == '(') || (c == ']' && inStack == '[') || (c == '}' && inStack == '{')) {
                    stack.pop();
                } else {
                    return false;
                }
            }
        }
        return stack.isEmpty();
    }
}